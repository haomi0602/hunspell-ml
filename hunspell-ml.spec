Name:        hunspell-ml
Summary:     Hunspell dictionaries for Malayalam
Version:     0.1
Release:     20
Source:      http://download.savannah.gnu.org/releases/smc/Spellchecker/ooo-hunspell-ml-%{version}.tar.bz2
URL:         http://download.savannah.gnu.org/releases/smc/Spellchecker/
License:     GPLv3+
BuildArch:   noarch

Requires:    hunspell
Supplements: (hunspell and langpacks-ml)

%description
Hunspell dictionaries for Malayalam

%prep
%autosetup -n ooo-hunspell-ml-%{version} -p1

%build
echo "Nothing to build..."

%install
install -d $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%files
%doc README COPYING
%{_datadir}/myspell/*

%changelog
* Tue Apr 14 2020 lizhenhua <lizhenhua21@huawei.com> - 0.1-20
- Package init
